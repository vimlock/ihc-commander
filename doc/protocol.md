

Communication between Arduino and Commander
===========================================

Conventions
-----------

All integers must encoded using network byte order (big endian).

Transmission layer
----------------

Communication between Arduino and Commander is implemented as
a packet based protocol.
Every package starts with a 0x2 byte and ends with a 0x3 byte.
After the package start there is a 1 byte wide checksum of the payload + sequence number.
Therefore a valid message is always 4 to 16 bytes in size.
Packages with invalid size are rejected with **ERR\_SIZE**.

Followed by the checksum is a 1 byte wide seq field.
Responses to requests from a party will always contain the same
sequence number.
This can be used to verify that response is in reply to a correct request.

The checksum, sequence number and payload can't contain bytes (0x2, 0x3, 0x1B),
as they need to be escaped in order to send them.
[See escaping for more info](#escaping).
Escape characters don't affect the size of a package.


| Start  | Checksum | Seq    | Payload      | End    |
| ------ | -------- | ------ | ------------ | ------ |
| 1 byte | 1 byte   | 1 byte | 0 - 12 bytes | 1 byte |


### <a name="escaping"></a> Escaping ###

Certain values are used as control sequences in the transmission protocol
and they can't appear in the package payload or checksum without escaping.

| Dec | Hex  | Description        |
| --- | ---- | ------------------ |
|   2 | 0x2  | Start of package   |
|   3 | 0x3  | End of package     |
|  27 | 0x1B | Escape             |

To send an escaped byte, take a bitwise XOR of the value with the escape code (0x1B)
and then send escape code before the escaped value in the serial port.
The escape code before an escaped character must no be escaped.gg

| Unescaped | Escaped   |
| --------- | --------- |
|   0x2     | 0x1B 0x25 |
|   0x3     | 0x1B 0x18 |
|   0x1B    | 0x1B 0x0  |

Note that only bytes 0x2, 0x3 and 0x1B can be escaped, and attempting to escape
anything else is an error and the receiver should respond the sender with **ERR\_ESCAPE**.

### Transmission layer errors ###

When an error occurs, the receiver will ignore everything until next start of a package.

If a any of the following errors occur, sender should try to resend the package.
If the sender does not receive reply in a while 1-5 ms, he should try to resend the package.
These errors can be caused by bad connection over wire and might not always be caused by
software bug. Keep in mind that resending packages in an infinite loop is not a good idea.
You should always implement retransmission with max retry count and abort.

| Code          |
| ------------- |
| ERR\_CHECKSUM |
| ERR\_ESCAPE   |
| ERR\_SIZE     |

Message layer
----------------

Message layer is built on top of the transmission layer.
The message protocol is somewhat like a binary HTTP protocol.
Messages are divided into 2 different categories, *requests* and *responses*.
Commander sends requests to Arduino and Arduino responds with responses.
Every request contains a request code which tells the recepient what action to perform.
Every response contains a response code which tells if the last request was successfull.
Requests and responses can contain additional data.

General structure of messages is presented in the following table.

| Request/Response code | Request/Response specific data |
| --------------------- | ------------------------------ |
| 1 byte                |  0 - 11 bytes                  |

### Request code list ###

General requests

| Code | Name                    | Description                                                          |
| ---- | ----------------------- | -------------------------------------------------------------------- |
|    0 | CMD\_STATUS             | Does nothing but can be used to see if an internal error has occured |
|    1 | CMD\_VERSION            | Returns version information. [See version info](#Version info)       |

Follwing request codes can be used on all channels

| Code | Name                     | Description                                                          |
| ---- | ------------------------ | -------------------------------------------------------------------- |
|    9 | CMD\_CONFIGURE\_CHANNEL  | Sets the control type of a channel. [See channel configuration](#Channel configuration)|
|   11 | CMD\_SET\_CHANNEL\_STATE | Set the channel state on/off
|   12 | CMD\_GET\_CHANNEL\_STATE | Return the state of the channel

Following request codes are LK 350 LR dimmer specific

| Code | Name                       | Description                                                       |
| ---- | -------------------------- | ----------------------------------------------------------------- |
|   16 | CMD\_LK350LR\_SET\_DIMMING | Set the target dimming value                                      |
|   17 | CMD\_LK350LR\_GET\_DIMMING | Get the target dimming value                                      |


### CMD\_STATUS ###

Request does not have any arguments.

Response does not have any data.

### CMD\_VERSION ###

Request does not have any arguments.

Response data

| Major version | Minor version |
| ------------- | ------------- |
| 1 byte        | 1 byte        |


### CMD\_CONFIGURE\_CHANNEL ###

Request

| Module | Channel | Control type |
| ------ | ------- | ------------ |
| 1 byte | 1 byte  | 1 byte       |

Response does not have any data.

Request specific errors

| Code         | Reason                           |
| ------------ | -------------------------------- |
| ERR\_MODULE  | Invalid module number            |
| ERR\_CHANNEL | Invalid channel number           |
| ERR\_CONTROL | Invalid control type             |


### CMD\_SET\_CHANNEL\_STATE ###

Request

| Module | Channel | State  |
| ------ | ------- | -----  |
| 1 byte | 1 byte  | 1 byte |

Response does not have any data.

Request specific errors

| Code         | Reason                           |
| ------------ | -------------------------------- |
| ERR\_MODULE  | Invalid module number            |
| ERR\_CHANNEL | Invalid channel number           |

### Control type list ###

| Code | Name                   | Description                                                       |
| ---- | ---------------------- | ----------------------------------------------------------------- |
|    1 | CTRL\_TOGGLE           | Standard on/off toggle                                            |
|    2 | CTRL\_LK350LR          | LK 350 LR Dimmmer                                                 |

### Response codes ###

| Code | Name               | Description                                     |
| ---- | ------------------ | ----------------------------------------------- |
|    0 | SUCCESS            | No error occured                                |
|    1 | ERR\_COMMAND       | Unknown command was sent                        |
|    2 | ERR\_CHECKSUM      | Checksum of the command does not match          |
|    3 | ERR\_MODULE        | Invalid module number was sent                  |
|    4 | ERR\_CHANNEL       | Invalid channel number was sent                 |
|    5 | ERR\_ESCAPE        | Received message was not escaped properly       |
|    6 | ERR\_SIZE          | Too large packet                                |
|  255 | ERR\_UNKNOWN       | Something went wrong...                         |





