
External links
==============

Info about serial devices
[http://www.tldp.org/HOWTO/Serial-HOWTO.html]

Virtual serial ports, can be used for testing
[http://www.dest-unreach.org/socat]

Reliable transfer over serial
[https://en.wikibooks.org/wiki/Serial_Programming/Error_Correction_Methods]
