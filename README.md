ihc-commander
==========

This is program is part of a project in Centria University of Applied Sciences
with a goal to implement system that uses existing IHC (Intelligent Home Control)
output modules to control lights and other targets.

Target of this program is to implement a bridge between Arduino and computer
it is connected to.
