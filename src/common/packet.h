#ifndef IHC_PACKET_H
#define IHC_PACKET_H

#include "arduino/arduino.h"

#define PACKET_MAX_PAYLOAD_SIZE 12

struct Packet
{
    byte dataSize;
    byte seq;
    byte data[PACKET_MAX_PAYLOAD_SIZE];
};

#endif /* IHC_PACKET_H */

