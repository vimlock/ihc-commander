#ifndef IHC_RELIABLE_SERIAL_H
#define IHC_RELIABLE_SERIAL_H

#include "arduino/arduino.h"
#include "packet.h"

class ReliableSerial
{
public:
    ReliableSerial(ISerialDevice *serial);

    void send(const Packet *packet);
    void send(byte seq, int dataSize, const byte *data);

    int recv(Packet *packet);

private:
    enum State {
        READ_START,
        READ_UNESCAPED,
        READ_ESCAPED
    };

    void appendByte(byte val);

    int endPacket(Packet *packet);
    void dropPacket();

    void writeEscaped(byte val);

    static byte makeChecksum(int bufsize, const byte *buf);

    ISerialDevice *serial;
    State state;
    byte buf[sizeof(Packet)];
    int bufNext;
    int packetsDropped;
    int packetsReceived;
};

#endif /* IHC_RELIABLE_SERIAL_H */

