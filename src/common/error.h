#ifndef IHC_ERROR_H
#define IHC_ERROR_H

#define SUCCESS       0
#define ERR_COMMAND   1
#define ERR_CHECKSUM  2
#define ERR_MODULE    3
#define ERR_CHANNEL   4
#define ERR_ESCAPE    5
#define ERR_SIZE      6
#define ERR_UNKNOWN 255

const char *err_str(int errcode);

#endif /* IHC_ERROR_H */

