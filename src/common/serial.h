#ifndef IHC_SERIAL_H
#define IHC_SERIAL_H

#include "arduino/types.h"

class ISerialDevice
{
public:
    virtual int available() const = 0;

    virtual int read() = 0;

    int readBuffer(byte *buf, int len)
    {
        int ch;
        int n = 0;

        while (n < len && (ch = read() >= 0)) {
            buf[n++] = ch;
        }

        return n;
    }

    virtual void write(byte val) = 0;

    void writeBuffer(const byte *buf, int len)
    {
        for (int i = 0; i < len; i++) {
            write(buf[i]);
        }
    }

private:
};

#endif /* ARDUINO_SERIAL_H */

