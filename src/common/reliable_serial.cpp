#include "reliable_serial.h"
#include "common/error.h"
#include <string.h>
#include <stdio.h>

#define PACKET_START  0x2
#define PACKET_END    0x3
#define ESCAPE_CODE 0x1B

static bool isControlByte(byte val)
{
    return val == PACKET_START || val == PACKET_END || val == ESCAPE_CODE;
}

static bool isEscapedControlByte(byte val)
{
    return
        val == (PACKET_START ^ ESCAPE_CODE) ||
        val == (PACKET_END ^ ESCAPE_CODE) ||
        val == (ESCAPE_CODE ^ ESCAPE_CODE);
}

ReliableSerial::ReliableSerial(ISerialDevice *serial):
    serial(serial),
    state(READ_START),
    bufNext(0),
    packetsDropped(0),
    packetsReceived(0)
{
}

void ReliableSerial::send(const Packet *packet)
{
    byte checksum = makeChecksum(packet->dataSize + 1, &packet->seq);

    serial->write(PACKET_START);
    writeEscaped(checksum);
    writeEscaped(packet->seq);
    for (int i = 0; i < packet->dataSize; i++) {
        writeEscaped(packet->data[i]);
    }
    serial->write(PACKET_END);
}

void ReliableSerial::send(byte seq, int dataSize, const byte *data)
{
    Packet packet;
    packet.seq = seq;
    packet.dataSize = dataSize;
    memcpy(packet.data, data, dataSize);

    send(&packet);
}

int ReliableSerial::recv(Packet *packet)
{
    while (true) {
        int c = serial->read();
        if (c < 0) {
            return 0;
        }

        if (c == PACKET_START) {
            if (state != READ_START) {
                fprintf(stderr, "unexpected package start\n");
                dropPacket();
            }

            state = READ_UNESCAPED;
        }
        else if (c == PACKET_END) {
            if (state == READ_UNESCAPED) {
                if (endPacket(packet)) {
                    return 1;
                }
            }
            else {
                fprintf(stderr, "unexpected package end\n");
                dropPacket();
            }
        }

        else if (c == ESCAPE_CODE) {
            if (state == READ_UNESCAPED) {
                state = READ_ESCAPED;
            }
            else {
                fprintf(stderr, "unexpected escape code\n");
                dropPacket();
            }
        }

        else {
            if (state == READ_UNESCAPED) {
                appendByte(c);
            }
            else if (state == READ_ESCAPED) {
                if (isEscapedControlByte(c)) {
                    appendByte(c ^ ESCAPE_CODE);
                    state = READ_UNESCAPED;
                }
                else {
                    fprintf(stderr, "bad escape sequence\n");
                    dropPacket();
                }
            }
            else if (state == READ_START) {
                /* do nothing */
                fprintf(stderr, "dropped byte\n");
            }
        }
    }
}

void ReliableSerial::appendByte(byte val)
{
    if (bufNext < (int)sizeof(buf)) {
        buf[bufNext++] = val;
    }
    else {
        fprintf(stderr, "too large packet\n");
        dropPacket();
    }
}

int ReliableSerial::endPacket(Packet *packet)
{
    if (bufNext < 3) {
        fprintf(stderr, "too small packet\n");
        dropPacket();
        return 0;
    }

    byte checkSum = buf[0];
    byte checkSrc = makeChecksum(bufNext - 1, buf + 1);

    if (checkSum != checkSrc) {
        fprintf(stderr, "bad checksum, %x != %x\n", checkSum, checkSrc);
        dropPacket();
        return 0;
    }

    packet->seq = buf[1];
    packet->dataSize = bufNext - 1;
    memcpy(packet->data, buf + 1, bufNext - 1);

    bufNext = 0;
    state = READ_START;
    packetsReceived++;
    fprintf(stderr, "received packet (count %d)\n", packetsReceived);

    return 1;
}

void ReliableSerial::dropPacket()
{
    state = READ_START;
    bufNext = 0;
    packetsDropped++;

    fprintf(stderr, "packet dropped (count %d)\n", packetsDropped);
}

void ReliableSerial::writeEscaped(byte val)
{
    if (isControlByte(val)) {
        serial->write(ESCAPE_CODE);
        serial->write(val ^ ESCAPE_CODE);
    }
    else {
        serial->write(val);
    }
}


byte ReliableSerial::makeChecksum(int bufSize, const byte *buf)
{
    byte remainder = 0;
    
    for (int i = 0; i < bufSize; i++) {
        // good enough
        remainder = buf[i] ^ remainder;
    }

    return remainder;
}
