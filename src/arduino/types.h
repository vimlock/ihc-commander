#ifndef ARDUINO_TYPES_H
#define ARDUINO_TYPES_H

#include <stdint.h>

typedef bool boolean;
typedef uint8_t byte;
typedef int word;

#endif /* ARDUINO_TYPES_H */

