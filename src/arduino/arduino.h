#ifndef ARDUINO_H
#define ARDUINO_H

#include <stdio.h>

#include "arduino/types.h"

#define LOW 0
#define HIGH 1

// the LOW constant is used here also
// #define LOW 0
#define CHANGE 1
#define RISING 2
#define FALLING 3

#define INPUT   0
#define OUTPUT  1

#define MAX_PINS 20

// not implemented
// #define INPUT_PULLUP 2

#include "common/serial.h"

class DummySerial : public ISerialDevice
{
public:
    DummySerial(FILE *in, FILE *out):
        in(in),
        out(out)
    { }

    void begin(int baudrate)
    {
        (void) baudrate;
        // do nothing
    }

    void end()
    {
        // do nothing
    }

    virtual int available() const;

    virtual int read();
    virtual void write(byte val);

private:
    FILE *in;
    FILE *out;
};

void pinMode(int pin, int mode);
int digitalRead(int pin);
void digitalWrite(int pin, int value);

void delay(unsigned long duration);
void delayMicroseconds(unsigned long duration);

unsigned long millis();
unsigned long micros();

void attachInterrupt( void (*handler)());

class DummySerial;
extern DummySerial Serial;

#endif /* ARDUINO_H */

