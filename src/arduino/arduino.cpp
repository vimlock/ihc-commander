#include "arduino.h"

#include <stddef.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>

struct Pin {
    Pin():
        state(INPUT),
        interruptHandler(NULL)
    {
    }

    int state; // HIGH or LOW
    int interruptMode; // LOW, CHANGE, RISING or FALLING
    void (*interruptHandler)();
};


int DummySerial::available() const
{
    int tmp = 0;
    if (ioctl(fileno(in), FIONREAD, &tmp) >= 0) {
        return tmp;
    }
    else {
        return 0;
    }
}

int DummySerial::read()
{
    int ch = fgetc(in);
    if (ch >= 0) {
        return ch;
    }
    else {
        return -1;
    }
}

void DummySerial::write(byte val)
{
    fputc(val, out);
}


DummySerial Serial(stdin, stdout);
static Pin pins[MAX_PINS];

void pinMode(int pin, int mode)
{
    if (pin < 0 || pin >= MAX_PINS) {
        return;
    }
}

int digitalRead(int pin)
{
}

void digitalWrite(int pin, int value)
{
}

void delay(unsigned long duration)
{
}

void delayMicroseconds(unsigned long duration)
{
}

void setup();
void loop();

int main(int argc, const char *argv[])
{
    (void)argc;
    (void)argv;

    fcntl(STDIN_FILENO, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);

    fprintf(stderr, "setup()\n");
    setup();
    
    fprintf(stderr, "loop()\n");
    while (1) {
        loop();
    }

    return 0;
}
