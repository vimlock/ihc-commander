#include "arduino.h"
#include "common/reliable_serial.h"
#include "common/error.h"

#include <string.h>
#include <stdlib.h>

ReliableSerial reliableSerial(&Serial);

#define CTRL_TOGGLE  1
#define CTRL_LK350LR 2

struct Channel {
    byte    controlType;
    byte    state;
};

struct Module {
    byte    pin;
    Channel channels[8];
};

struct Module **modules = NULL;
const int numModules = 1;

void sendResponse(byte seq, byte respcode, int dataSize, const byte *data)
{
    Packet packet;
    packet.seq = seq;
    packet.data[0] = respcode;
    memcpy(packet.data + 1, data, dataSize);
    reliableSerial.send(&packet);
}

void handlePacket(const Packet *packet)
{
    if (packet->dataSize <= 0) {
        sendResponse(packet->seq, ERR_COMMAND, 0, NULL);
        return;
    }

    byte command = packet->data[0];
    sendResponse(packet->seq, SUCCESS, 0, NULL);
}

Module *makeModule(int pin)
{
    Module *m = (Module *)malloc(sizeof(Module));
    m->pin = pin;

    for (int i = 0; i < 8; i++) {
        m->channels[i].controlType = CTRL_TOGGLE;
        m->channels[i].state = 0;
    }

    return m;
}

void delayNanoseconds(long duration)
{
    // left as exercise for the reader
}

void update_module_state(Module *module)
{
    register int pin = module->pin;
    register Channel *channels = module->channels;

    // module pins should be at high state but lets just make sure
    digitalWrite(pin, HIGH);
    delayMicroseconds(41000);

    digitalWrite(pin, LOW);
    delayMicroseconds(300);

    for (register int i = 0; i < 8; i++) {
        int highDuration = channels[i].state ? 150 : 300;

        digitalWrite(pin, HIGH);
        delayMicroseconds(highDuration);
        digitalWrite(pin, LOW);
        delayMicroseconds(600 - highDuration);
    }
}

void setup()
{
    Serial.begin(9600);

    modules = (struct Module **)malloc(sizeof(struct Module *) * numModules);
    for (int i = 0; i < numModules; i++) {
        modules[i] = makeModule(2 + i);
    }

    for (int i = 0; i < numModules; i++) {
        update_module_state(modules[i]);
    }
}

void loop()
{
    Packet packet;
    if (reliableSerial.recv(&packet)) {
        handlePacket(&packet);
    }
}
