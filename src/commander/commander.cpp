#include <errno.h>
#include <string.h>

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "commander/tty_serial.h"
#include "common/reliable_serial.h"

void print_usage(const char *progname, const struct option *options, const char **helps)
{
    fprintf(stderr, "usage: %s [OPTIONS]\n", progname);

    const struct option *opt = options;
    while (opt && opt->name) {
        fprintf(stderr, "\t-%c\t--%-10s%s\n", opt->val, opt->name, *helps);
        opt++;
        helps++;
    }
    
    fprintf(stderr, "\n");
    fprintf(stderr, "author: Joel Polso <joel.polso@gmail.com>\n");
}

int main(int argc, const char *argv[])
{
    static struct option long_opt[] = {
        {"baudrate",  required_argument, NULL, 'b'},
        {"help",      no_argument,       NULL, 'h'},
        {"tty",       required_argument, NULL, 't'},
        {NULL, 0, NULL, 0}
    };

    static const char *helps[] = {
        "set the baudrate, 9600 by default",
        "show this help",
        "set the serial port to connect, this argument is required"
    };

    int baudrate = 9600;
    const char *tty_path = NULL;

    int c;
    while ((c = getopt_long(argc, (char *const *)argv, "hb:t:", long_opt, NULL)) != -1) {
        switch (c) {
        case 'b': {
            char *endptr;
            baudrate = strtol(optarg, &endptr, 10);
            if (*endptr != '\0') {
                fprintf(stderr, "baudrate not an integer\n");
                return EXIT_FAILURE;
            }
            break;
        }
        case 'h':
            print_usage(argv[0], long_opt, helps);
            return EXIT_FAILURE;
        case 't':
            tty_path = optarg;
            break;
        case ':':
        case '?':
            fprintf(stderr, "see %s --help for list of options\n", argv[0]);
            return EXIT_FAILURE;
        }
    }

    if (!tty_path) {
        fprintf(stderr, "serial port not given\n");
        fprintf(stderr, "see %s --help for list of options\n", argv[0]);
        return EXIT_FAILURE;
    }

    TTYSerial serial(tty_path, baudrate);
    ReliableSerial reliableSerial(&serial);

    for (int i = 0; i < 5; i++) {
        reliableSerial.send(i, 5, (byte *)"hello");
    }

    fprintf(stderr, "success\n");

    return EXIT_SUCCESS;
}
