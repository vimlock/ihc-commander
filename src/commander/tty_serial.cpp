#include "tty_serial.h"

#include <string.h>
#include <stdio.h>

#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <exception>

static void init_serial(int fd, int baudrate, int parity)
{
    struct termios tty;

    memset(&tty, '\0', sizeof(tty));
    if (tcgetattr(fd, &tty) < 0) {
        fprintf(stderr, "failed to get tty attributes: %s\n", strerror(errno));
        throw std::exception();
    }

    switch (baudrate) {
    case 300:    baudrate = B300;    break;
    case 600:    baudrate = B600;    break;
    case 1200:   baudrate = B1200;   break;
    case 2400:   baudrate = B2400;   break;
    case 4800:   baudrate = B4800;   break;
    case 9600:   baudrate = B9600;   break;
    case 19200:  baudrate = B19200;  break;
    case 38400:  baudrate = B38400;  break;
    case 115200: baudrate = B115200; break;
    default:
        fprintf(stderr, "invalid baudrate: %d\n", baudrate);
        throw std::exception();
    }

    cfsetospeed(&tty, baudrate);
    cfsetispeed(&tty, baudrate);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
    tty.c_iflag &= ~IGNBRK;
    tty.c_lflag = 0;

    tty.c_oflag = 0;
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 0;

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty.c_cflag |= (CLOCAL | CREAD);
    tty.c_cflag &= (PARENB | PARODD);
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr(fd, TCSANOW, &tty) < 0) {
        fprintf(stderr, "failed to set tty attributes: %s\n", strerror(errno));
        throw std::exception();
    }
}

TTYSerial::TTYSerial(const char *devicePath, int baudrate)
{
    fd = open(devicePath, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        fprintf(stderr, "failed to open '%s': %s\n", devicePath, strerror(errno));
        throw std::exception();
    }

    try {
        if (!isatty(fd)) {
            fprintf(stderr, "'%s' is not a TTY\n", devicePath);
            throw std::exception();
        }

        init_serial(fd, baudrate, 0);
    } catch (...) {
        close(fd);
        throw;
    }
}

TTYSerial::~TTYSerial()
{
    close(fd);
}

int TTYSerial::available() const
{
    int tmp = 0;
    if (ioctl(fd, FIONREAD, &tmp) >= 0) {
        return tmp;
    }
    else {
        return 0;
    }
}

int TTYSerial::read()
{
    char buf = 0;
    int n = ::read(fd, &buf, 1);
    if (n > 0) {
        return buf;
    }
    else {
        return -1;
    }
}

void TTYSerial::write(byte val)
{
    ::write(fd, &val, 1);
}
