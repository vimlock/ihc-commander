#ifndef IHC_TTY_SERIAL_H
#define IHC_TTY_SERIAL_H

#include "common/serial.h"

class TTYSerial : public ISerialDevice
{
public:
    TTYSerial(const char *devicePath, int baudrate);
    ~TTYSerial();

    virtual int available() const;
    virtual int read();
    virtual void write(byte val);

private:
    int fd;

};

#endif /* IHC_TTY_SERIAL_H */

