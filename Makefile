
CC = g++
RM = rm -f

CPPFLAGS =-g -Wall -Wextra
LDFLAGS  =

INC=-Isrc

OBJDIR = build

vpath %.cpp src/arduino src/common src/commander
vpath %.ino src/arduino

arduino_objs = $(addprefix $(OBJDIR)/, arduino.o controller.o)
commander_objs = $(addprefix $(OBJDIR)/, commander.o tty_serial.o)
common_objs = $(addprefix $(OBJDIR)/, reliable_serial.o)

all: arduino commander

arduino: $(common_objs) $(arduino_objs)
	$(CC) $(CFLAGS) -o arduino $(common_objs) $(arduino_objs) 

commander: $(common_objs) $(commander_objs)
	$(CC) $(CFLAGS) -o commander $(common_objs) $(commander_objs)

$(OBJDIR)/controller.o: src/arduino/controller.ino $(OBJDIR)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(INC) -x c++ -c -o $@ $<

$(OBJDIR)/%.o: %.cpp $(OBJDIR)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(INC) -c -o $@ $<


$(OBJDIR):
	@mkdir -p $@

.phony: clean

clean:
	$(RM) build/*
